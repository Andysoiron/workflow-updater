# frozen_string_literal: true

module Regex
  def self.sep_by_1(re, sep)
    %r{ #{re} (?: #{sep} #{re} )* }x
  end
end

