# frozen_string_literal: true

require 'delegate'

require_relative '../regex.rb'
require_relative './issue.rb'

class WorkflowUpdater
  class MergeRequest < SimpleDelegator
    WORKFLOW_SCOPE = /workflow::.*/

    TRACKER_ISSUES_START = /<!--tracking_issues_start/
    TRACKER_ISSUES_END = /tracking_issues_end-->/
    SLUG_RE  = %r{ [A-Za-z]+ }x
    PATH_SEG = Regex.sep_by_1(SLUG_RE, '-')
    PATH_RE = Regex.sep_by_1(PATH_SEG, '/')
    IID_RE = / (?: (?<ref> #{PATH_RE} \# \d+) | \#? (?<iid> \d+) ) /x
    OPTIONAL_WS = /\s*/
    COMMA_OR_WS = /(,|\s)/
    SEPARATOR = /#{COMMA_OR_WS} #{OPTIONAL_WS}/x
    RELATED_TO_RE = / ^ (?:[Rr]elated \s to|[Ff]ixes|[Ss]ee) :? \s+ (?<iids> #{IID_RE}) /x

    def initialize(api, merge_request)
      super(merge_request)

      @api = api
    end

    def workflow
      return @workflow if defined?(@workflow)

      @workflow = labels.find { |label| WORKFLOW_SCOPE.match(label) }
    end

    def ensure_correct_workflow_label!
      return workflow if workflow_correct?

      @api.update_merge_request(project_id, iid, { add_labels: suggested_workflow })
      @workflow = suggested_workflow
    end

    def mislabelled_issues
      issue_iids.group_by(&:first).flat_map do |ns, grp|
        iids = grp.map(&:last)

        @api.issues(ns, { iids: iids, not: { labels: [workflow] } })
      end
    end

    def issue_iids
      @iids ||= [RELATED_TO_RE, related_issues_section_re, tracker_issues_regex].flat_map { extract_iids(_1) }.uniq
    end

    private

    def workflow_correct?
      return false unless workflow
      return false if in_dev?    && !['workflow::blocked', 'workflow::in dev'].include?(workflow)
      return false if in_review? && 'workflow::in review' != workflow
      return false if done?      && ['workflow::in dev', 'workflow::in review'].include?(workflow)

      true
    end

    def suggested_workflow
      return 'workflow::in dev'       if in_dev?
      return 'workflow::in review'    if in_review?
      return 'workflow::verification' if done?

      workflow
    end

    def in_dev?
      state == 'opened' && (draft || reviewers.empty?)
    end

    def in_review?
      state == 'opened' && !draft && reviewers.any?
    end

    def done?
      ['merged', 'closed'].include?(state)
    end

    def extract_iids(re)
      re_scan(description, re).flat_map { re_scan(_1, IID_RE).map { |s| with_ns(s) } }
    end

    def re_scan(str, re)
      str.scan(re).flat_map(&:compact)
    end

    def with_ns(str)
      parts = str.split('#')
      return [project_id, parts.first] if parts.length == 1

      parts
    end

    # This is probably too strict, since it does not allow text
    def tracker_issues_regex
      /
        #{TRACKER_ISSUES_START}
        #{OPTIONAL_WS}
        (?<iids>
          #{Regex.sep_by_1(IID_RE, SEPARATOR)}
        )
        #{OPTIONAL_WS}
        #{TRACKER_ISSUES_END}
      /mx
    end

    def related_issues_section_re
      newline = /\n/

      /
        ^ \#\# \s+
        (?: Related \s [Ii]ssues | RELATED \s ISSUES | related \s issues ) #{newline}
        (?: #{newline})+

        (?<iids>
        (?: - \s #{IID_RE} #{newline} )+
        )

        (?: #{newline})+
      /mx
    end
  end
end
