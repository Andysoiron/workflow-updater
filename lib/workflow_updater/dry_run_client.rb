# frozen_string_literal: true

require 'delegate'

class WorkflowUpdater
  class DryRunClient < SimpleDelegator
    def edit_merge_request(ns, iid, opts)
      warn("Updating #{ns}!#{iid} => #{opts}")
    end

    def edit_issue(ns, iid, opts)
      warn("Updating #{ns}\##{iid} => #{opts}")
    end
  end
end
