# frozen_string_literal: true

require 'delegate'

class WorkflowUpdater
  class Issue < SimpleDelegator
    def initialize(api, issue)
      super(issue)

      @api = api
    end

    def transition(workflow)
      @api.edit_issue(project_id, iid, { add_labels: workflow })
    end
  end
end
