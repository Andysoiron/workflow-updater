# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'gitlab'

require 'active_support'
require 'active_support/core_ext/time'

require './lib/workflow_updater/merge_request'
require './lib/workflow_updater/dry_run_client'

class WorkflowUpdater
  MRS_LIMIT = 10

  def initialize(projects:, api_token:, dry_run: false, period: nil)
    @projects = projects
    @token = api_token
    @dry_run = dry_run
    @period = period
  end

  def execute
    warn "Considering MRs in #{@projects}"

    merge_requests.each do |mr|
      mr.ensure_correct_workflow_label!

      mr.mislabelled_issues.each do |issue|
        warn "Transitioning #{issue.references.full} to #{mr.workflow}"
        Issue.new(client, issue).transition(mr.workflow)
      end
    end
  end

  private

  def merge_requests
    @projects.flat_map do |project|
      gitlab
        .merge_requests(project, **mr_query)
        .map { WorkflowUpdater::MergeRequest.new(client, _1) }
    end
  end

  def mr_query
    @mr_query ||= { updated_after: updated_after&.iso8601, scope: 'assigned_to_me', order_by: 'updated_at', per_page: MRS_LIMIT }.compact
  end

  def updated_after
    return if @period.nil?

    @updated_after ||= (Time.current - ActiveSupport::Duration.parse(@period))
  end

  def client
    return DryRunClient.new(gitlab) if @dry_run

    gitlab
  end

  def gitlab
    @client ||= Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: @token)
  end
end
