#!/usr/bin/env ruby

require 'optparse'
require 'dotenv/load'

require File.expand_path("../lib/workflow_updater", __dir__)

options = {
  projects: ENV.fetch('GITLAB_PROJECT', '').split(/,/),
  api_token: ENV['GITLAB_TOKEN'],
  period: ENV['GITLAB_UPDATE_PERIOD'],
  dry_run: false
}

OptionParser.new do |opts|
  opts.banner = "Usage: workflow_updater [options]"

  opts.on("-d", "--[no-]dry-run", "Dry run. Do not update issues.") do |v|
    options[:dry_run] = v
  end

  opts.on('--project [PROJECT_PATH]', 'Run against a project') do |path|
    options[:projects] << path
  end

  opts.on('--api-token [TOKEN]', 'Private API token') do |token|
    options[:api_token] = token
  end

  opts.on('--period [DURATION]', 'Time since the last run') do |duration|
    options[:period] = duration
  end

  opts.on('-h', '--help', 'Print usage information') do
    puts opts
    exit 1
  end
end.parse!

abort "Missing required argument: projects" if options[:projects].empty?
abort "Missing required argument: api_token" unless options[:api_token]

WorkflowUpdater.new(**options).execute
