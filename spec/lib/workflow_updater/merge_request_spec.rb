# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/workflow_updater/merge_request'

RSpec.describe WorkflowUpdater::MergeRequest do
  subject(:instance) { described_class.new(client, mr) }

  let(:mr) { double(:MergeRequest, project_id: 'XXX', iid: 'MRIID', state: 'opened') }
  let(:client) { double(:Client) }

  describe 'ensure_correct_workflow_label!' do
    context 'the MR has no workflow label' do
      before do
        allow(mr).to receive(:labels).and_return([])
      end

      context 'the MR is in draft' do
        before do
          allow(mr).to receive(:draft).and_return(true)
        end

        it 'updates the workflow label to workflow::in dev' do
          expect(client).to receive(:update_merge_request)
            .with('XXX', 'MRIID', { add_labels: 'workflow::in dev'})

          expect { instance.ensure_correct_workflow_label! }
            .to change(instance, :workflow).to('workflow::in dev')
        end
      end

      context 'the MR is in review' do
        before do
          allow(mr).to receive(:draft).and_return(false)
          allow(mr).to receive(:reviewers).and_return([:x])
        end

        it 'updates the workflow label to workflow::in review' do
          expect(client).to receive(:update_merge_request)
            .with('XXX', 'MRIID', { add_labels: 'workflow::in review'})

          expect { instance.ensure_correct_workflow_label! }
            .to change(instance, :workflow).to('workflow::in review')
        end
      end
    end

    context 'the MR is labelled as being in dev' do
      before do
        allow(mr).to receive(:labels).and_return(['workflow::in dev'])
      end

      context 'the MR is in draft' do
        before do
          allow(mr).to receive(:draft).and_return(true)
        end

        it 'does not update the workflow label' do
          expect { instance.ensure_correct_workflow_label! }.not_to change(instance, :workflow)
        end
      end

      context 'the MR is in review' do
        before do
          allow(mr).to receive(:draft).and_return(false)
          allow(mr).to receive(:reviewers).and_return([:x])
        end

        it 'updates the workflow label to workflow::in review' do
          expect(client).to receive(:update_merge_request)
            .with('XXX', 'MRIID', { add_labels: 'workflow::in review'})

          expect { instance.ensure_correct_workflow_label! }
            .to change(instance, :workflow).to('workflow::in review')
        end
      end
    end

    context 'the MR is labelled as being in review' do
      before do
        allow(mr).to receive(:labels).and_return(['workflow::in review'])
      end

      context 'the MR is in draft' do
        before do
          allow(mr).to receive(:draft).and_return(true)
        end

        it 'updates the workflow label to workflow::in dev' do
          expect(client).to receive(:update_merge_request)
            .with('XXX', 'MRIID', { add_labels: 'workflow::in dev'})

          expect { instance.ensure_correct_workflow_label! }
            .to change(instance, :workflow).to('workflow::in dev')
        end
      end

      context 'the MR is in review' do
        before do
          allow(mr).to receive(:draft).and_return(false)
          allow(mr).to receive(:reviewers).and_return([:x])
        end

        it 'does not update the workflow label' do
          expect { instance.ensure_correct_workflow_label! }.not_to change(instance, :workflow)
        end
      end
    end
  end

  describe 'mislabelled_issues' do
    context 'the MR has no related issues' do
      before do
        allow(subject).to receive(:issue_iids).and_return([])
      end

      it 'returns an empty array' do
        expect(subject).to have_attributes(mislabelled_issues: be_empty)
      end
    end

    context 'there are some related issues, in one or more namespaces' do
      before do
        allow(mr).to receive(:labels).once.and_return(%w[foo workflow::wibble bar baz])
        allow(subject).to receive(:issue_iids).and_return [
          ['foo', '123'],
          ['bar', '456'],
          ['foo', '789'],
          ['baz', '001']
        ]
      end

      it 'calls the the API once for each namespace, and concatenates the results' do
        params = { not: { labels: ['workflow::wibble'] } }

        expect(client).to receive(:issues).once.with('foo', params.merge(iids: %w[123 789])) { [:a] }
        expect(client).to receive(:issues).once.with('bar', params.merge(iids: %w[456])) { [:b] }
        expect(client).to receive(:issues).once.with('baz', params.merge(iids: %w[001])) { [:c] }

        expect(subject.mislabelled_issues).to contain_exactly(:a, :b, :c)
      end
    end
  end

  describe '#workflow' do
    context 'the MR has no labels' do
      before do
        allow(mr).to receive(:labels).once.and_return([])
      end

      it { is_expected.to have_attributes(workflow: be_nil) }

      it 'memoizes the result' do
        subject.workflow
        subject.workflow
      end
    end

    context 'the MR has only irrelevant labels' do
      before do
        allow(mr).to receive(:labels).once.and_return(%w[foo bar baz])
      end

      it { is_expected.to have_attributes(workflow: be_nil) }
    end

    context 'the MR has a workflow label' do
      before do
        allow(mr).to receive(:labels).once.and_return(%w[foo workflow::wibble bar baz])
      end

      it { is_expected.to have_attributes(workflow: eq('workflow::wibble')) }
    end
  end

  describe 'issue_iids' do
    before do
      allow(mr).to receive(:description).and_return(description)
    end

    context 'when the description contains tracking_issues' do
      let(:description) do
        <<~DESC
          Some text

          <!--tracking_issues_start 123, 456,789 101 #102 tracking_issues_end-->

          Some more text
        DESC
      end

      it 'finds all issue IIDs' do
        expected = %w[123 456 789 101 102].map { ['XXX', _1] }

        expect(subject.issue_iids).to match_array(expected)
      end
    end

    context 'when the description mentions fully qualified issues' do
      let(:description) do
        <<~DESC
          Some text

          Related to: foo-bar/baz#1000
        DESC
      end

      it 'finds the qualified issue IID' do
        expect(subject.issue_iids).to contain_exactly(['foo-bar/baz', '1000'])
      end
    end

    context 'when the description includes a RELATED ISSUES section' do
      let(:description) do
        <<~DESC
          Some text

          This came out of work on foo#123 and #1234

          ## Related issues
          
          - foo-bar/baz#1000
          - baz#1001
          - #1002

          ## Unrelated issues

          - foo-bar/baz#1001
          - baz#2001
          - #2002
        DESC
      end

      it 'finds listed issues' do
        expected = [
          ['foo-bar/baz', '1000'],
          ['baz', '1001'],
          ['XXX', '1002']
        ]

        expect(subject.issue_iids).to match_array(expected)
      end
    end

    context 'when the description includes several ways of mentioning issues' do
      let(:description) do
        <<~DESC
          Some text

          <!--tracking_issues_start 123, 456,789 101 #102 tracking_issues_end-->

          Some more text

          Related to: #1000
          Fixes: #999
          Does not fix #998
          related to: #1001
          Related to #1002
          Fixes wibble/bing#1234
          Related to our search for meaning in a cold dark universe (also: 42)
          Not related to #2000
          See: #12345
          See: quux#12345
        DESC
      end

      it 'finds all issue IIDs' do
        expected = %w[123 456 789 101 102 1000 1001 1002 999 12345].map { ['XXX', _1] }
        expected << ['wibble/bing', '1234']
        expected << ['quux', '12345']

        expect(subject.issue_iids).to match_array(expected)
      end
    end
  end
end
