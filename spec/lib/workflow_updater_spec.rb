# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/workflow_updater'

RSpec.describe WorkflowUpdater do
  subject { workflow_updater.execute }

  let(:workflow_updater) { WorkflowUpdater.new(projects: projects, api_token: token) }
  let(:projects) { [5, 'foo-bar'] }
  let(:token) { '123' }
  let(:labels) { ['workflow::in dev'] }
  let(:description) { 'Foo bar <!--tracking_issues_start 1,2 tracking_issues_end-->' }
  let(:mrs_json) do
    [
      {
        project_id: 5,
        id: 199,
        draft: false,
        reviewers: [],
        state: 'opened',
        labels: labels,
        description: description
      }
    ].to_json
  end
  let(:issues_json) do
    [
      { project_id: 5, iid: 'A', reference: 'foo-bar#A', references: { full: 'foo-bar#A' } }
    ].to_json
  end

  let(:encoded_issues_query) do
    'iids%5B%5D=1&iids%5B%5D=2&not%5Blabels%5D%5B0%5D=workflow::in%20dev'
  end

  def mr_query_uri(project_id)
    "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests?scope=assigned_to_me&order_by=updated_at&per_page=10"
  end

  before do
    stub_request(:get, mr_query_uri(5))
      .with(headers: { 'Private-Token'=>'123' })
      .to_return(status: 200, body: mrs_json, headers: {})

    stub_request(:get, mr_query_uri('foo-bar'))
      .with(headers: { 'Private-Token'=>'123' })
      .to_return(status: 200, body: '[]', headers: {})

    stub_request(:get, 'https://gitlab.com/api/v4/projects/5/issues?' + encoded_issues_query)
      .to_return(status: 200, body: issues_json, headers: {})

    stub_request(:put, 'https://gitlab.com/api/v4/projects/5/issues/A')
      .to_return(status: 200, body: '', headers: {})
  end

  it 'fetches labels from an MR and adds them to the tracking issues' do
    subject

    expect(WebMock).to have_requested(:get, mr_query_uri(5))
    expect(WebMock).to have_requested(:get, mr_query_uri('foo-bar'))

    expect(WebMock).to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/A')
      .with(body: { 'add_labels' => 'workflow::in dev' })
  end

  it 'writes logs' do
    expect(workflow_updater).to receive(:warn).with("Considering MRs in #{projects}")
    expect(workflow_updater).to receive(:warn).with("Transitioning foo-bar#A to workflow::in dev")

    subject
  end

  context 'when MR description does not reference a tracker issue' do
    let(:description) { '' }

    specify do
      expect(WebMock).not_to have_requested(:put, 'https://gitlab.com/api/v4/projects/5/issues/1')
      expect{ subject }.to_not raise_error
    end
  end
end
